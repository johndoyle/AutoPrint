<h4>Simple monitor to print PDFs appearing in a folder to the default printer and then delete file after.</h4>

Used by us to print invoices as they are downloaded (created) to the default printer. They are deleted afterwards

This is a upload of a netBeans project with all the source. The jar file should be put in a directory with the following structure

./AutoPrint.jar <br/>
./run.bat <br/>
./logs/ <br/>
./config/config.properties <br/>
./lib/pdfbox-app-2.0.4.jar <br/>

Configuration of the service is found in the config.properties file.  The example version is found in the "working" folder and there is an helpful run.bat for all the windows folks.
