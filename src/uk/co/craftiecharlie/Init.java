/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.craftiecharlie;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.util.Date;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import uk.co.craftiecharlie.model.FileConnect;
import uk.co.craftiecharlie.view.MainWindow;
import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
import uk.co.craftiecharlie.model.Invoices;
/**
 *
 * @author John
 */
public final class Init {
    
    private final FileConnect fc = new FileConnect();
    private String fileStartsWith;
    private String fileEndsWith;
    private WatchService watcher;
    private Map<WatchKey,Path> keys;
    private final Invoices invoice = new Invoices();
    
    public Init() {
        initLogger();
        showWindow();
        startDirectoryMonitor();

        // Capture new file added
        // Check if printerable type
        // send to printer if printable
            
    }
//    private void initDirectoryMonitor() {

    public void startDirectoryMonitor() {
        Path monitorDir = null;
        // Sanity check - Check if path is a folder
        try {
            monitorDir = Paths.get(fc.getProperty("monitorDir"));
            Boolean isFolder = (Boolean) Files.getAttribute(monitorDir,
                                "basic:isDirectory", NOFOLLOW_LINKS);
            if (!isFolder) {
                Logger.getLogger("Path: " + monitorDir + " is not a folder");
                throw new IllegalArgumentException("Path: " + monitorDir + " is not a folder");
            }
        } catch (IOException ioe) {
            // Folder does not exists
            ioe.printStackTrace();
        }

        Logger.getLogger("Watching path: " + monitorDir);
        // We obtain the file system of the Path
        FileSystem fs = monitorDir.getFileSystem ();

        // We create the new WatchService using the new try() block
        try(WatchService service = fs.newWatchService()) {

            fileStartsWith = fc.getProperty("fileStartsWith");
            fileEndsWith = fc.getProperty("fileEndsWith");
            Logger.getLogger("File mask : " + fileStartsWith + "*" + fileEndsWith);
            // We register the path to the service
            // We watch for creation events
            monitorDir.register(service, ENTRY_CREATE);

            // Start the infinite polling loop
            WatchKey key = null;
            while(true) {
                key = service.take();

                // Dequeueing events
                Kind<?> kind = null;
                for(WatchEvent<?> watchEvent : key.pollEvents()) {
                    // Get the type of the event
                    kind = watchEvent.kind();
                    if (OVERFLOW == kind) {
                            continue; //loop
                    } else if (ENTRY_CREATE == kind) {
                        // A new Path was created 
                        Path newPath = ((WatchEvent<Path>) watchEvent).context();
                        // Output
//                        if (newPath.toString().startsWith("Print") && newPath.toString().endsWith("txt")) {
                        if (newPath.toString().startsWith(fileStartsWith) && newPath.toString().endsWith(fileEndsWith)) {
                            try{
                                Path dir = null;
                                dir = Paths.get(monitorDir.toString(), newPath.toString());
                                invoice.addInvoice(newPath);
//                                System.out.println("Printing:" + newPath);
                                Logger.getLogger("Printing:" + newPath);
                                printInvoice(dir);
                                invoice.removeInvoice(dir);
                                this.invoice.notifyObservers();
                            } catch (PrintException ex) {
                                Logger.getLogger(Init.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } else {
                            Logger.getLogger("New file " + newPath + "ignored");
//                            System.out.println("New path Ignored: " + newPath);
                        }
                    }
                }

                if(!key.reset()) {
                        break; //loop
                }
            }
        } catch(IOException ioe) {
                ioe.printStackTrace();
        } catch(InterruptedException ie) {
                ie.printStackTrace();
        }

    }
    private void printInvoice(Path path) throws PrintException {
        try {
//            printerName = fc.getProperty("printerName");
            PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
            DocPrintJob printJob = printService.createPrintJob();

            PDDocument pdDocument = PDDocument.load(path.toFile());
            PDFPageable pdfPageable = new PDFPageable(pdDocument);
            SimpleDoc doc = new SimpleDoc(pdfPageable, DocFlavor.SERVICE_FORMATTED.PAGEABLE, null);

            printJob.print(doc, null);
            pdDocument.close();
        } catch (IOException ex) {
            Logger.getLogger(Init.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void showWindow() {
        MainWindow mainWindow = new MainWindow();
        invoice.addObserver(mainWindow);

        /* Create and display the window */
        int widthWindow = mainWindow.getWidth();
        int heightWindow = mainWindow.getHeight();
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        int X = (screen.width / 4) - (widthWindow / 4); // Center horizontally.
        int Y = (screen.height / 3) - (heightWindow / 3); // Center vertically.

        mainWindow.setBounds(X,Y , widthWindow,heightWindow);
        mainWindow.showPanel();

    }
    private void initLogger() {
        try {
            String systemName = fc.getProperty("systemName");
            //String path;
            CodeSource codeSource = Init.class.getProtectionDomain().getCodeSource();
            File jarFile = new File(codeSource.getLocation().toURI().getPath());
            //path = jarFile.getParentFile().getPath();
            String fullPath = fc.getProperty("logPath") + "AutoPrintLog." + systemName + ".%g." + "log.txt";

            Handler handler = new FileHandler(fullPath, 65536, 80, false);
            Logger.getLogger("").addHandler(handler);
            SimpleFormatter formatter = new SimpleFormatter();
            handler.setFormatter(formatter);
            String startMsg = "Applications Start : " + new Date();
            Logger.getLogger(Init.class.getName()).log(Level.INFO, startMsg);
            
        } catch (NullPointerException ex) {
            Logger.getLogger(Init.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Init.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | SecurityException ex) {
            Logger.getLogger(Init.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(Init.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
