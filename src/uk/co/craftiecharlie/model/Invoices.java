/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.craftiecharlie.model;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

/**
 *
 * @author John
 */
public class Invoices extends Observable {
    
    private Boolean             printed = Boolean.FALSE;  //  has been printed
    private ArrayList<Observer> observers = new ArrayList<Observer>();
    private String              invoiceName;
    
    public Invoices() {
    }

    public void addInvoice(Path path) {
        this.invoiceName = path.toString();
        notifyObserver(invoiceName);
    }
    public void removeInvoice(Path path) {
        this.invoiceName = "";
        path.toFile().delete();
        Logger.getLogger("Deleting:" + path);
        System.out.println("Deleting: " + path);
        notifyObserver(invoiceName);
    }
    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }
    public void notifyObserver(String i) {
        for(Observer observer :observers) {
            observer.update(this, i);
        }
    }
}
