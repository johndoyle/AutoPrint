/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.craftiecharlie.model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author John
 */
public class FileConnect {

    public FileConnect() {
    }

    /**
    * Gets a property value from
    * the ./config.properties file of the base folder
    *
    * @param property
    * @return property string
    * @throws java.io.FileNotFoundException
    */
    public String getProperty(String property) throws FileNotFoundException {
        String value = null;
        String configPath = "E:/Development/AutoPrint/build/classes";
        try {   
            //to load application's properties, we use this class
            Properties mainProperties = new Properties();
            FileInputStream file;
            //the base folder is ./, the root of the main.properties file  
            String path = configPath + "/config/config.properties";
            //load the file handle for main.properties
            file = new FileInputStream(path);
            //load all the properties from this file
            mainProperties.load(file);
            //we have loaded the properties, so close the file handle
            file.close();
            //retrieve the property we are intrested, the app.version
            value = mainProperties.getProperty(property);
        } catch (FileNotFoundException ex) {
            throw new FileNotFoundException("FileConnect.getProperty(): Configuration file not found");
        } catch (IOException ex) {
            Logger.getLogger(FileConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return value;
    }
       
}
